import argparse
import csv
import json
from copy import copy
from datetime import datetime
from sys import maxsize

from Airport import Airport
from Flight import Flight

flights = []
airports = {}
airports_dict = {}
paths = []
glob_flight_paths = []
args = None


def parse_args():
    parser = argparse.ArgumentParser(description='Find flights between two airports')
    parser.add_argument('--origin', type=str, help="origin airport code", required=True)
    parser.add_argument('--destination', type=str, help="destination airport code", required=True)
    parser.add_argument('--data', type=str, help="path to a file", required=True)
    parser.add_argument('--output', type=str, help="path to a file", required=False)
    parser.add_argument('--bags', type=int, default=0, help="number of bags that you want to carry", required=False)
    parser.add_argument('--return-trip', '--return', action=argparse.BooleanOptionalAction, default=False,
                        help="if you want to find return flight")
    parser.add_argument('--max-price', type=int, default=maxsize, help="max price for your trip", required=False)

    args = parser.parse_args()
    return args


def get_total_price(flight_list):
    total_price = 0
    for flight in flight_list:
        total_price += flight.get_total_price(args.bags)
    return total_price


def handle_output(result):
    if args.output:
        with open(args.output, "w+") as csvfile:
            csvfile.write(json.dumps(result))
    else:
        print(result)


def serialize_result(origin, destination):
    result = []
    for flight_combos in glob_flight_paths:
        if len(flight_combos) == 0:
            handle_output(result)
            return
        flights_serialize = []
        bags_count = flight_combos[0].bags_allowed
        start_time = datetime.fromisoformat(flight_combos[0].departure)
        end_time = 0
        total_price = get_total_price(flight_combos)
        for flight in flight_combos:
            flights_serialize.append(flight.serialize())
            bags_count = min(bags_count, flight.bags_allowed)
            end_time = datetime.fromisoformat(flight.arrival)
        time_delta = abs(end_time - start_time)
        if total_price > args.max_price:
            continue
        result.append({
            "flights": flights_serialize,
            "bags_allowed": bags_count,
            "bags_count": args.bags,
            "destination": destination,
            "origin": origin,
            "total_price": total_price,
            "travel_time": str(time_delta)
        })
    result = sorted(result, key=lambda d: d['total_price'])
    handle_output(result)


def prepare_rows(header, data):
    row_dict = {}
    for index, col in enumerate(header):
        row_dict[col] = data[index]
    return row_dict


def read_files():
    with open(args.data, newline='') as csvfile:
        flight_info = csv.reader(csvfile, delimiter=' ', quotechar='\'', skipinitialspace=True)
        header = next(flight_info)
        header = header[0].split(",")
        for row in flight_info:
            row_dict = prepare_rows(header, row[0].split(","))
            flights.append(Flight(row_dict))
            if row_dict['origin'] not in airports:
                airports[row_dict['origin']] = Airport(row_dict['origin'])
            if row_dict['destination'] not in airports:
                airports[row_dict['destination']] = Airport(row_dict['destination'])


def is_possible_layover(flight_dep, flight_arr):
    flight_dep = datetime.fromisoformat(flight_dep.departure)
    flight_arrival = datetime.fromisoformat(flight_arr.arrival)
    diff = abs(flight_dep - flight_arrival)
    diff_hours = diff.seconds / 3600
    if not ((1 < diff_hours < 6) and flight_dep > flight_arrival):
        return False
    return True


def meet_requirements(flight):
    all_met = True
    if not int(flight.bags_allowed) >= args.bags:
        return False
    return all_met


def find_route(airports, origin, destination, path=[], flight_paths=[],
               departure_time=datetime(1900, 1, 1)):
    """
    Recursive function for finding path from origin to destination airport
    :param airports: list of know airports from csv
    :param origin: code of origin airport
    :param destination: code of destination airport
    :param path: airport codes
    :param flight_paths: flights
    :param departure_time: departure time for flight
    :return:
    """
    path = path + [origin]
    if origin == destination:
        paths.append(path)
        glob_flight_paths.append(flight_paths)
        return path, flight_paths
    if origin not in airports.keys():
        return None
    for flight in airports[origin].departures:
        if not meet_requirements(flight):
            continue
        if flight.destination not in path:
            if datetime.fromisoformat(flight.departure) < departure_time:
                continue
            if len(flight_paths) > 0:
                if not (is_possible_layover(flight, flight_paths[-1])):
                    continue
            find_route(airports, flight.destination, destination, path, flight_paths + [flight])
    return None


def add_flights_to_airports():
    for flight in flights:
        airports[flight.origin].departures.append(flight)
        airports[flight.destination].arrivals.append(flight)


if __name__ == "__main__":
    args = parse_args()
    read_files()
    add_flights_to_airports()
    route = find_route(airports, args.origin, args.destination)
    if args.return_trip:
        return_flights = []
        one_way_flights = copy(glob_flight_paths)
        glob_flight_paths = []
        return_groups = []
        for flight_groups in one_way_flights:
            find_route(airports, args.destination, args.origin,
                       departure_time=datetime.fromisoformat(flight_groups[-1].arrival))

            ###connecting flights
            for flight_return_groups in glob_flight_paths:
                inner = []
                inner.extend(flight_groups)
                inner.extend(flight_return_groups)
                return_groups.append(inner)
            glob_flight_paths = []

        glob_flight_paths = return_groups

    serialize_result(args.origin, args.destination)
