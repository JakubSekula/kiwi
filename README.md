# Kiwi

## How to run
You should start the script by running python(>v3.6) Main.py.
Parameters for the script are

    --data - path to file with flights - required
    --origin - string with origin airport code - required
    --destination - string with destination airport code - required
    --bags - int with maximal -> default = 0
    --return - boolean if you want to get return flights aswell
    --max-price - int with max price that you are willing to pay for one way trip 
    --output - file into which the result is gonna be written, default is stdout

examples:

    python Main.py --data=inputs/example1.csv --origin=WIW --destination=PRG --return
    python Main.py --data=inputs/example1.csv --origin=WIW --destination=PRG
    python Main.py --data=inputs/example1.csv --origin=WIW --destination=PRG --bags=1
