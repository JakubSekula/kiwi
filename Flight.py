class Flight():
    def __init__(self, args):
        self.flight_no = args['flight_no']
        self.origin = args['origin']
        self.destination = args['destination']
        self.departure = args['departure']
        self.arrival = args['arrival']
        self.base_price = args['base_price']
        self.bag_price = args['bag_price']
        self.bags_allowed = args['bags_allowed']
        self.route = []

    def get_total_price(self, bags):
        return float(self.base_price) + float(self.bag_price) * float(bags)

    def serialize(self):
        return {
            "flight_no": self.flight_no,
            "origin": self.origin,
            "destination": self.destination,
            "departure": self.departure,
            "arrival": self.arrival,
            "base_price": self.base_price,
            "bag_price": self.bag_price,
            "bags_allowed": self.bags_allowed
        }
